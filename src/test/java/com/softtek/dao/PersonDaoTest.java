package com.softtek.dao;

import com.softtek.beans.Person;
import com.softtek.configuration.JDBCConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JDBCConfiguration.class})
@WebAppConfiguration
public class PersonDaoTest {
    @Qualifier("personRepository")
    @Autowired
    private PersonDao personDao;

    @Test
    public void testGetAllPersons(){
        //setup
        Person person = new Person("aldaTest1",122,1);

        //execute
        List<Person> persons = personDao.getAll();

        //verify
        assertNotNull(persons);
        Person actualPerson = persons.get(0);
        assertEquals(person.getName(),actualPerson.getName());
    }
}
