package com.softtek.dao;

import com.softtek.beans.Person;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class PersonJDBCImpl implements PersonDao {

    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Person> getAll() {
        String SQL_SELECT = "Select * from Person";
        List<Person> result = jdbcTemplate
                .query(SQL_SELECT, (rs, rowNum) -> {
                    Person person = new Person(
                            rs.getString("name"),
                            rs.getInt("age"),
                            rs.getInt("idPerson")
                    );
                    return person;
                });
        return result;
    }

    @Override
    public void addPerson(Person person) {

    }

    @Override
    public Person updatePerson(int id) {
        return null;
    }


    @Override
    public void deletePerson(int id) {

    }

    @Override
    public void updatePersonByObject(Person person) {
        
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
