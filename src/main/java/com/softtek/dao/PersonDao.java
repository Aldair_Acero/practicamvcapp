package com.softtek.dao;

import com.softtek.beans.Person;

import java.util.List;

public interface PersonDao{
    List<Person> getAll();
    void addPerson(Person person);
    Person updatePerson(int id);
    void deletePerson(int id);
    void updatePersonByObject(Person person);
}