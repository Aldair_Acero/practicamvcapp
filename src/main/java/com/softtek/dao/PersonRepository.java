package com.softtek.dao;

import com.softtek.beans.Person;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transaction;
import java.util.List;

@Repository("personRepository")
public class PersonRepository implements PersonDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Person> getAll() {
        return entityManager.createQuery(
                "SELECT p FROM Person p",
                Person.class
        ).getResultList();
    }

    @Override
    @Transactional
    public void addPerson(Person person) {
        entityManager.persist(person);
    }

    @Override
    public Person updatePerson(int id) {
        return entityManager.find(Person.class,id);
    }

    @Transactional
    @Override
    public void deletePerson(int id) {
        Person person = entityManager.find(Person.class,id);
        entityManager.remove(person);
    }

    @Override
    @Transactional
    public void updatePersonByObject(Person person) {
        entityManager.merge(person);
    }


}
