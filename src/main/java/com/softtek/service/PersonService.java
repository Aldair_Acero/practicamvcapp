package com.softtek.service;
import com.softtek.beans.Person;
import org.springframework.stereotype.Service;

import java.util.List;


public interface PersonService{

    List<Person> getAll();
    void addPerson(Person person);
    Person updatePerson(int id);
    void deletePerson(int id);
    void updatePersonByObject(Person person);
}